namespace :db do
  task :fetch_data, [:db_name, :db_user, :db_pwd] do |t, args| 
    args.with_defaults(:db_name => "ep_dev", :db_user => `printf $USER`, :db_pwd => :default_2)
    fetch_data(args[:db_name], args[:db_user], args[:db_pwd])
  end
end

private
def fetch_data(db_name="ep_dev", db_user=`printf $USER`, db_pwd="")
  initial_time = Time.now
  db_host = "localhost"

  db = PG::Connection.new(:host => db_host, :dbname => db_name, :user => db_user, :password => db_pwd)

  body = HTTP.get("http://www.camara.leg.br//SitCamaraWS/Deputados.asmx/ObterDeputados").to_s
  body_hash = Hash.from_xml(body)
  parliamentarians = body_hash['deputados']['deputado']

  puts parliamentarians.count.to_s + " parlamentares encontrados!"

 # client = Savon.client do
 #   wsdl "http://www.camara.gov.br/SitCamaraWS/Proposicoes.asmx?wsdl"
 #   open_timeout 10
 #   read_timeout 10
 #   log false
 # end

  proposition_types = [
    'PL', 'PLC', 'PLN', 'PLP', 'PLS', 'PLV', 'EAG', 'EMA', 'EMC', 'EMC-A',
    'EMD', 'EML', 'EMO', 'EMP', 'EMPV', 'EMR', 'EMRP', 'EMS', 'EPP', 'ERD',
    'ERD-A', 'ESB', 'ESP', 'PEC', 'PDS', 'PDN', 'PDC'
  ]

  parliamentarians.each do |p|
    id = p['ideCadastro']
    puts id
    condition = p['condicao']
    registry = p['matricula']
    civil_name = p['nome'].to_s.gsub("'", "''").mb_chars.titleize
    name = p['nomeParlamentar'].to_s.gsub("'", "''").mb_chars.titleize
    url_photo = p['urlFoto']
    state = p['uf']
    party = p['partido']
    cabinet = p['gabinete']
    phone = p['fone']
    email = p['email']

    puts
    puts "Obtendo proposições de " + "#{name}"

    begin
      insert = db.query("INSERT INTO parliamentarians VALUES (#{id}, '#{condition}',
         '#{registry}', '#{civil_name}', '#{name}', '#{url_photo}', '#{state}',
                        '#{party}', '#{cabinet}', '#{phone}', '#{email}')")
    rescue
      puts name.to_s + " já cadastrado"
    end

    proposition_types.each do |t|
      puts 'passo1'
      puts t
      begin
        puts name
        #puts '1'*100
        puts 'passo2'
        body = HTTP.get("https://dadosabertos.camara.leg.br/api/v2/proposicoes?siglaTipo=#{t}&ordem=ASC&ordenarPor=id").to_s
        #puts "http://www.camara.leg.br/SitCamaraWS/Proposicoes.asmx/ListarProposicoes?sigla=#{t}&numero=&ano=&datApresentacaoIni=&datApresentacaoFim=&parteNomeAutor=#{name}&idTipoAutor=&siglaPartidoAutor=&siglaUFAutor=&generoAutor=&codEstado=&codOrgaoEstado=&emTramitacao="
        #puts '2'*100
        puts 'passo3'
        
        body = Hash.from_xml(body)
        puts 'passo4'
        puts body
        propositions =  body['proposicao']
#        "http://www.camara.leg.br/SitCamaraWS/Proposicoes.asmx/ListarProposicoes?sigla=#{t}&numero=&ano=&datApresentacaoIni=&datApresentacaoFim=&parteNomeAutor=#{name}&idTipoAutor=&siglaPartidoAutor=&siglaUFAutor=&generoAutor=&codEstado=&codOrgaoEstado=&emTramitacao="
        #body = HTTP.get("http://www.camara.leg.br/SitCamaraWS/Proposicoes.asmx/ListarProposicoes?sigla=#{t}&numero=&ano=&datApresentacaoIni=&datApresentacaoFim=&parteNomeAutor=#{name}&idTipoAutor=&siglaPartidoAutor=&siglaUFAutor=&generoAutor=&codEstado=&codOrgaoEstado=&emTramitacao=").to_s
        #body = Hash.from_xml(body)

        #response = client.call(:listar_proposicoes) do
        #  message( sigla: t,  parteNomeAutor: name )
        #end
        propositions.each do |pr|
          puts pr
          pr_id = pr['id']
          proposition_type = t
          number = pr['numero']
          year = pr['ano']
          presentation_date = pr['datApresentacao'].to_datetime.strftime("%Y-%m-%d %H:%M:%S")
          amendment = pr['txtEmenta']
          explanation = pr['txtExplicacaoEmenta']
          parliamentarian_id = pr['autor1']['idecadastro']
          situation = pr['situacao']['descricao']

          #puts "http://www.camara.leg.br//SitCamaraWS/Proposicoes.asmx/ObterProposicaoPorID?IdProp=#{pr_id}"
          #body = HTTP.get("http://www.camara.leg.br//SitCamaraWS/Proposicoes.asmx/ObterProposicaoPorID?IdProp=#{pr_id}").to_s
        #  body = HTTP.get("https://dadosabertos.camara.leg.br/api/v2/proposicoes?siglaTipo=#{pr_id}&ordem=ASC&ordenarPor=id").to_s
        #  body = Hash.from_xml(body)



          extra_information = body['proposicao']

          content_link = extra_information['linkInteiroTeor']
          themes = extra_information['tema'].split('; ')

          begin
            insert = db.query("INSERT INTO propositions VALUES (#{pr_id}, '#{proposition_type}',
                              #{number}, #{year}, '#{presentation_date}', '#{amendment}', '#{explanation}',
                '#{situation}', '#{content_link}', #{parliamentarian_id})")
          rescue
            puts "Proposição " + pr_id + " já cadastrada."
          end

          themes.each do |th|
            begin
              insert = db.query( "INSERT INTO themes (description) VALUES ('#{th}')" )
            rescue
              puts "Tema " + th + " já cadastrado."
            end

            begin
              th_id = db.query( "SELECT id from themes WHERE (description = '#{th}')" ).to_a[0]["id"]
              insert = db.query( "INSERT INTO propositions_themes VALUES (#{pr_id}, #{th_id})" )
            rescue
              puts "Relacionamento Tema-Proposição já existente"
            end
          end
        end
      rescue
        puts "parlamentares sem proposição de " + t.to_s
      end
    end
  end
    db.close

    final_time = Time.now
    time_spent = Time.parse(final_time.to_s) - Time.parse(initial_time.to_s)

    puts ""
    puts "O parser levou: " + (time_spent/3600%24).to_int.to_s + "h" + ((time_spent/60)%60).to_int.to_s + "min e " + (time_spent%60).to_int.to_s + "seg para ser concluído!"
end
